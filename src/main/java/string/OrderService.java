package string;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@SuppressWarnings("unused")
public class OrderService {

	private DataSource dataSource;

	public OrderService(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public List<Order> getFilledOrders() {

		List<Order> filledOrders = new ArrayList<Order>();

		for (Order order : dataSource.getOrders()) {
			if (order.isFilled()) {
				filledOrders.add(order);
			}
		}

		return filledOrders;
	}

	public List<Order> getOrdersOver(double amount) {

		List<Order> ordersOver = new ArrayList<Order>();

		for (Order order : dataSource.getOrders()) {
			if (amount < order.getTotal()) {
				ordersOver.add(order);
			}
		}

		return ordersOver;
	}

	public List<Order> getOrdersSortedByDate() {

		List<Order> ordersSortedByDate = new ArrayList<Order>(dataSource.getOrders());

		Collections.sort(ordersSortedByDate, new Comparator<Order>() {

			@Override
			public int compare(Order o1, Order o2) {
				return o1.getOrderDate().compareTo(o2.getOrderDate());
			}
		});

		return ordersSortedByDate;
	}
}
