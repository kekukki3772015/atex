package refactoring;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class RefactoringTests {

	private Refactoring sut = new Refactoring();

	@Test
	public void getsItemsAsHtml() throws Exception {
		assertThat(sut.getItemsAsHtml(), is("<ul><li>1</li><li>2</li><li>3</li><li>4</li></ul>"));
	}

	@Test
	public void calculatesWeeklyPayWithOvertime() {
		assertThat(sut.calculateWeeklyPayWithOvertime(47), is(253));
		assertThat(sut.calculateWeeklyPayWithOvertime(41), is(208));
	}

	@Test
	public void calculatesWeeklyPayWithoutOvertime() {
		assertThat(sut.calculateWeeklyPayWithoutOvertime(17), is(85));
		assertThat(sut.calculateWeeklyPayWithoutOvertime(39), is(195));
	}
}
