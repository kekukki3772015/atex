package refactoring;

import java.util.*;

public class Refactoring {

	// 1a
	public int increaseInvoiceNumberByOne() {
		return ++invoiceNumber;
	}

	// 1b
	public void addNewOrder(List<Order> orderList) {
		for (Order order : orders) {
			if (order.isFilled()) {
				orderList.add(order);
			}
		}
	}

	// 2a
	public void printStatementFor(Long invoiceId) {
		List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);

		printInvoiceRows(invoiceRows);

		double total = getInvoiceTotal(invoiceRows);

		printValue(total);
	}

	private double getInvoiceTotal(List<InvoiceRow> invoiceRows) {
		double total = 0;
		for (InvoiceRow invoiceRow : invoiceRows) {
			total += invoiceRow.getAmount();
		}
		return total;
	}

	// 2b
	public String getItemsAsHtml() {
		String retval = "";

		for (String item : Arrays.asList(item1, item2, item3, item4)) {
			retval += encloseInTag(item, "li");
		}

		return encloseInTag(retval, "ul");
	}

	public String encloseInTag(String content, String tag) {
		content = "<" + tag + ">" + content + "</" + tag + ">";
		return content;
	}

	// 3
	public boolean isSmallOrder() {
		return (order.getTotal() > 100);
	}

	// 4
	public void printPrice() {
		System.out.println("Hind ilma käibemaksuta: " + getBasePrice());
		System.out.println("Hind käibemaksuga: " + getBasePrice() * TURNOVER_TAX);
	}

	static final double TURNOVER_TAX = 1.2;

	// 5
	public void calculatePayFor(Job job) {
		if (overtimeWork(job)) {

		} else {

		}
	}

	private boolean overtimeWork(Job job) {
		return (workingAtNight(job) || workingOnWeekend(job));
	}

	private boolean workingAtNight(Job job) {
		return (job.hour > 20 || job.hour < 7);
	}

	private boolean workingOnWeekend(Job job) {
		return (job.day == 6 || job.day == 7);
	}

	// 6
	public boolean canAccessResource(SessionData sessionData) {
		return (hasPreferredStatus(sessionData) && isAdmin(sessionData));
	}

	private boolean hasPreferredStatus(SessionData sessionData) {
		return (sessionData.getStatus().equals("preferredStatusX")
				|| sessionData.getStatus().equals("preferredStatusY"));
	}

	private boolean isAdmin(SessionData sessionData) {
		return (sessionData.getCurrentUserName().equals("Admin")
				|| sessionData.getCurrentUserName().equals("Administrator"));
	}

	// 7
	public void drawLines() {
		Space space = new Space();

		Point startPointOne = new Point(12, 3, 5);
		Point endPontOne = new Point(2, 4, 6);
		Line LineOne = new Line(startPointOne, endPontOne);
		space.drawLine(LineOne);

		Point startPointTwo = new Point(2, 4, 6);
		Point endPontTwo = new Point(0, 1, 0);
		Line LineTwo = new Line(startPointTwo, endPontTwo);
		space.drawLine(LineTwo);
	}

	// 8
	public int calculateWeeklyPayWithoutOvertime(int hoursWorked) {
		return hoursWorked * hourRate;
	}

	public int calculateWeeklyPayWithOvertime(int hoursWorked) {
		int overTime = (int) Math.round((hoursWorked - straightTime) * overTimeHourRate);
		return calculateWeeklyPayWithoutOvertime(straightTime) + overTime;
	}

	// //////////////////////////////////////////////////////////////////////////

	// Abiväljad ja abimeetodid.
	// Need on siin lihtsalt selleks, et kood kompileeruks

	private String item1 = "1";
	private String item2 = "2";
	private String item3 = "3";
	private String item4 = "4";
	private int hourRate = 5;
	private double overTimeHourRate = hourRate * 1.5;
	private int straightTime = 40;
	int invoiceNumber = 0;
	private List<Order> orders = new ArrayList<Order>();
	private Order order = new Order();
	private Dao dao = new SampleDao();
	private double price = 0;

	void justACaller() {
		increaseInvoiceNumberByOne();
		addNewOrder(null);
	}

	private void printValue(double total) {
	}

	private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
	}

	class Space {
		public void drawLine(Line line) {
			// Draw the line
		}
	}

	public class Line {
		public Point startPoint;
		public Point endPoint;

		public Line(Point startPoint, Point endPoint) {
			this.startPoint = startPoint;
			this.endPoint = endPoint;
		}
	}

	public class Point {
		public int x;
		public int y;
		public int z;

		public Point(int x, int y, int z) {
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}

	interface Dao {
		List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
	}

	class SampleDao implements Dao {
		@Override
		public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
			return null;
		}
	}

	class Order {
		public boolean isFilled() {
			return false;
		}

		public double getTotal() {
			return 0;
		}
	}

	class InvoiceRow {
		public double getAmount() {
			return 0;
		}
	}

	class Job {
		public int hour;
		public int day;
	}

	private double getBasePrice() {
		return price;
	}

	private class SessionData {

		public String getCurrentUserName() {
			return null;
		}

		public String getStatus() {
			return null;
		}

	}

}
