package order;

import java.util.ArrayList;
import java.util.List;

public class Data implements DataSource {

	private List<Order> orders = new ArrayList<Order>();

	public Data(List<Order> orders) {
		this.orders = orders;
	}

	@Override
	public List<Order> getOrders() {
		return orders;
	}

}
