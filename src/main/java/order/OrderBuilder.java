package order;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class OrderBuilder {

	private boolean filled = false;
	private String number;
	private Date date;
	private LineItem lineItem;

	public OrderBuilder withNumber(String number) {
		this.number = number;
		return this;
	}

	public OrderBuilder withDate(String dateAsString) {
		try {
			this.date = new SimpleDateFormat("yyyy-MM-dd").parse(dateAsString);
			return this;
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	public OrderBuilder addItem(String name, double price) {
		this.lineItem = new LineItem(name, (int) price);
		return this;
	}

	public OrderBuilder isFilled() {
		this.filled = true;
		return this;
	}

	public Order build() {
		Order o = new Order(number, date);
		o.setFilled(this.filled);
		o.addItem(lineItem);
		
		return o;
	}
}