package order;

import java.util.ArrayList;
import java.util.List;

public class OrderServiceBuilder {

	private List<Order> orders = new ArrayList<Order>();
	
    public OrderServiceBuilder add(OrderBuilder orderBuilder) {
    	orders.add(orderBuilder.build());
    	return this;
    }

    public OrderService build() {
    	OrderService o = new OrderService(new Data(orders));
    	return o;
    }
}