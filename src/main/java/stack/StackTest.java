package stack;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;

@SuppressWarnings("unused")
public class StackTest {

	@Test
	public void newStackHasNoElements() {
		Stack stack = new Stack(100);
		assertThat(stack.getSize(), is(0));
	}

	@Test
	public void pushIncreaseSize() {
		Stack stack = new Stack(100);
		stack.push(1);
		stack.push(2);
		assertThat(stack.getSize(), is(2));
	}

	@Test
	public void pushDecreaseSize() {
		Stack stack = new Stack(100);
		stack.push(1);
		stack.push(2);
		stack.pop();
		stack.pop();
		assertThat(stack.getSize(), is(0));
	}

	@Test
	public void popReturnsPushedElements() {
		Stack stack = new Stack(100);
		stack.push(1);
		stack.push(2);

		// System.out.println(Arrays.asList(stack.stack));

		assertThat(stack.pop(), is(2));
		assertThat(stack.pop(), is(1));
	}

	@Test
	public void peekTopMostElem() {
		Stack stack = new Stack(100);
		stack.push(1);
		stack.push(2);
		stack.peek();
		assertThat(stack.peek(), is(2));
	}

	@Test
	public void peekTopMostElemAndGetSize() {
		Stack stack = new Stack(100);
		stack.push(1);
		stack.push(2);
		stack.peek();
		assertThat(stack.peek(), is(2));
		assertThat(stack.getSize(), is(2));
	}

	@Test
	public void peekTopMostTwice() {
		Stack stack = new Stack(100);
		stack.push(1);
		stack.push(2);
		stack.peek();
		stack.peekAgin();
		assertThat(stack.peek(), is(2));
		assertThat(stack.peekAgin(), is(2));
	}

	@Test(expected = IllegalStateException.class)
	public void popIfEmpty() {
		Stack stack = new Stack(100);
		stack.pop();
	}

	@Test(expected = IllegalStateException.class)
	public void peekIfEmpty() {
		Stack stack = new Stack(100);
		stack.peek();
	}
}

class Stack {

	private int size = 0;
	private int peek;
	static Integer[] stack;

	public Stack(int maxSize) {
		stack = new Integer[maxSize];
	}

	public Integer peekAgin() {
		if (stack[size - 1] != peek) {
			throw new IllegalArgumentException();
		} else
			return peek;
	}

	public Integer peek() {
		if (size == 0)
			throw new IllegalStateException();
		peek = stack[size - 1];
		return peek;
	}

	public Integer pop() {
		if (size == 0)
			throw new IllegalStateException();
		size--;
		return stack[size];
	}

	public void push(int elem) {
		stack[size] = elem;
		size++;
	}

	public Integer getSize() {
		return size;
	}

}