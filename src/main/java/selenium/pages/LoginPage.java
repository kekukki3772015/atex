package selenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends AbstractPage {

    public LoginPage(WebDriver driver) {
        super(driver);
        if (elementById("login_page") == null) {
            throw new IllegalStateException("not on login page");
        }
    }

    public AbstractPage logInWith(String user, String pass) {

        // sisestage kasutajanimi
        // sisestage salasõna
        // vajutage sisse logimise nupule



        if (getErrorMessage() != null) {
            // kui oli viga, siis tagastage sisselogimise leht
        }

        // tagastage menüü lehe objekt

        return null;
    }

    public String getErrorMessage() {
        WebElement element = elementById("error_message");
        return element == null ? null : element.getText();
    }

    public static LoginPage goTo() {
        WebDriver driver = getDriver();
        driver.get(BASE_URL);
        return new LoginPage(driver);
    }

}
