package selenium;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import selenium.pages.*;

@SuppressWarnings("unused")
public class Selenium {

    private static final String USERNAME = "user";
    private static final String CORRECT_PASSWORD = "1";
    private static final String WRONG_PASSWORD = "2";

    private WebDriver driver = new HtmlUnitDriver();

    @After
    public void closeDriver() {
        driver.close();
    }

    @Test
    public void loginFailsWithFalseGredentials() {
        LoginPage loginPage = LoginPage.goTo();

        loginPage.logInWith(USERNAME, WRONG_PASSWORD);

        assertThat(loginPage.getErrorMessage(), is(notNullValue()));
    }

    @Test
    public void sampleTest() {
        driver.get("http://enos.itcollege.ee/~mkalmo/selenium");

        elementById("username_box").sendKeys("user");
        elementById("password_box").sendKeys("1");

        elementById("log_in_button").click();

        elementById("show_users_link").click();

        List<WebElement> rows = elementById("user_list")
                .findElements(By.tagName("div"));

        for (WebElement row : rows) {
            System.out.println(row.getAttribute("username"));
            System.out.println(row.getAttribute("password"));
        }

        System.out.println(driver.getPageSource());
    }

    public WebElement elementById(String id) {
        List<WebElement> elements = driver.findElements(By.id(id));
        return elements.isEmpty() ? null : elements.get(0);
    }

}
