package common;

public interface BankService {

    void credit(Money money, String fromAccount);

    void debit(Money money, String toAccount);

    Money convert(Money money, String targetCurrency);

    String getAccountCurrency(String account);

    boolean hasSufficientFundsFor(Money requiredAmount, String account);
}