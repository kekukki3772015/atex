package common;

public class TransferService {

    private BankService bankService;

    public TransferService(BankService bankService) {
        this.bankService = bankService;
    }

    public void transfer(Money money, String fromAccount, String toAccount) {

        // konverteeri summa krediteeritava konto valuutassse
        // ja krediteeri

        String fromCurrency = bankService.getAccountCurrency(fromAccount);
        System.out.println(fromCurrency);
        Money creditAmountConverted = bankService.convert(money, fromCurrency);
        System.out.println(creditAmountConverted);

        if (!bankService.hasSufficientFundsFor(creditAmountConverted, fromAccount))
            return;

        bankService.credit(creditAmountConverted, fromAccount);

        // konverteeri summa debiteeritava konto valuutassse
        // ja debiteeri

        String toCurrency = bankService.getAccountCurrency(toAccount);
        Money debitAmountConverted = bankService.convert(money, toCurrency);
        System.out.println(debitAmountConverted);
        bankService.debit(debitAmountConverted, toAccount);
    }
}