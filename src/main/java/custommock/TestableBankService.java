package custommock;

import java.util.Arrays;
import java.util.List;

import common.BankService;
import common.Money;

public class TestableBankService implements BankService {

	private List<Object> creditArguments;
	private List<Object> debitArguments;
	private boolean sufficientFunds = true;
	private boolean isCreditCalled;

	public boolean wasCreditCalledWith(Money money, String account) {
		if (this.creditArguments.get(0).equals(money) && this.creditArguments.get(1).equals(account))
			return true;
		else
			return false;
	}

	public boolean wasDebitCalledWith(Money money, String account) {
		if (this.debitArguments.get(0).equals(money) && this.debitArguments.get(1).equals(account))
			return true;
		else
			return false;
	}

	@Override
	public void credit(Money money, String fromAccount) {
		this.creditArguments = Arrays.asList(money, fromAccount);
		this.isCreditCalled = true;
	}

	@Override
	public void debit(Money money, String toAccount) {
		this.debitArguments = Arrays.asList(money, toAccount);
	}

	@Override
	public Money convert(Money money, String targetCurrency) {
		if (money.getCurrency().equals(targetCurrency))
			return money;

		double rate = 1.0 / 10;

		return new Money((int) (money.getAmount() / rate), targetCurrency);
	}

	@Override
	public String getAccountCurrency(String account) {
		switch (account) {
		case "E_123":
			return "EUR";
		case "S_456":
			return "SEK";
		default:
			throw new IllegalStateException();
		}
	}

	@Override
	public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
		return this.sufficientFunds;
	}

	public void setSufficentFundsAvailable(boolean sufficientFunds) {
		this.sufficientFunds = sufficientFunds;
	}

	public boolean wasCreditCalled() {
		return this.isCreditCalled;
	}

}