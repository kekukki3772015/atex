package mockito;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import common.BankService;
import common.TransferService;
import common.Money;

@SuppressWarnings("unused")
public class TransferServiceTestMockito {

	BankService bankService = mock(BankService.class);
	TransferService transferService = new TransferService(bankService);

	@Test
	public void transferWithCurrencyConversion() {

		when(bankService.getAccountCurrency("E_123")).thenReturn("EUR");
		when(bankService.getAccountCurrency("S_456")).thenReturn("SEK");

		when((bankService).convert(new Money(1, "EUR"), "EUR")).thenReturn(new Money(1, "EUR"));
		when((bankService).convert(new Money(1, "EUR"), "SEK")).thenReturn(new Money(2, "SEK"));

		when(bankService.hasSufficientFundsFor(new Money(1, "EUR"), "E_123")).thenReturn(true);

		transferService.transfer(new Money(1, "EUR"), "E_123", "S_456");
		verify(bankService).convert(new Money(1, "EUR"), "EUR");

		verify(bankService).credit(new Money(1, "EUR"), "E_123");
		verify(bankService).debit(new Money(2, "SEK"), "S_456");
	}

	@Test
	public void transferWhenNotEnoughFunds() {
		when(bankService.hasSufficientFundsFor(anyMoney(),anyString())).thenReturn(false);
		verify(bankService, never()).credit(anyMoney(), anyString());
	}

	private Money anyMoney() {
		return (Money) any();
	}

	private String anyAccount() {
		return anyString();
	}
}