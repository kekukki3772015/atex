package matcher;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

public class MatcherTest {

	@Test
	public void test() {
		assertThat(1, isLesserThan(2));
		assertThat(2, is(IsGreaterThan(1)));
		assertThat(1, is(not(IsGreaterThan(2))));
	}

	public class IsLesserThanMatcher extends TypeSafeMatcher<Integer> {

		private Integer expected;

		public IsLesserThanMatcher(Integer expected) {
			this.expected = expected;
		}

		@Override
		public void describeTo(Description description) {
			description.appendText("greater than " + expected);
		}

		@Override
		protected boolean matchesSafely(Integer actual) {
			return actual < expected;
		}

	}

	private IsLesserThanMatcher isLesserThan(int value) {
		return new IsLesserThanMatcher(value);
	}

	public class IsGreaterThanMatcher extends TypeSafeMatcher<Integer> {

		private Integer expected;

		public IsGreaterThanMatcher(Integer expected) {
			this.expected = expected;
		}

		@Override
		public void describeTo(Description description) {
			description.appendText("greater than " + expected);
		}

		@Override
		protected boolean matchesSafely(Integer actual) {
			return actual > expected;
		}

	}

	private IsGreaterThanMatcher IsGreaterThan(int value) {
		return new IsGreaterThanMatcher(value);
	}

}
