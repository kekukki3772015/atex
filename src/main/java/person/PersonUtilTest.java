package person;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.*;

import org.junit.Test;

public class PersonUtilTest {

	private PersonUtil personUtil = new PersonUtil();

	@Test
	public void findsOldestPerson() {
		Person p1 = aPerson().withAge(32).build();
		Person p2 = aPerson().withAge(55).build();
		Person p3 = aPerson().withAge(21).build();

		assertThat(personUtil.getOldest(asList(p1, p2, p3)), is(p2));
	}

	@Test
	public void findsPersonsInLegalAge() {
		Person p1 = aPerson().withAge(17).build();
		Person p2 = aPerson().withAge(18).build();

		assertThat(personUtil.getPersonsInLegalAge(asList(p1, p2)), is(asList(p2)));
	}

	@Test
	public void findsWomen() {
		Person p1 = aPerson().withGender(Person.GENDER_MALE).build();
		Person p2 = aPerson().withGender(Person.GENDER_FEMALE).build();

		assertThat(personUtil.getWomen(asList(p1, p2)), is(asList(p2)));
	}

	@Test
	public void findsPersonsLivingInSpecifiedTown() {
		Person p1 = aPerson().withTown("Tartu").build();
		Person p2 = aPerson().withTown("Pärnu").build();

		assertThat(personUtil.getPersonsWhoLiveIn("Pärnu", asList(p1, p2)), is(asList(p2)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void getOldestIfListIsEmpty() {
		List<Person> emptyList = new ArrayList<Person>();
		personUtil.getOldest(emptyList);
	}

	@Test(expected = IllegalArgumentException.class)
	public void getOldestIfListIsNull() {
		List<Person> emptyList = null;
		personUtil.getOldest(emptyList);
	}

	private PersonBuilder aPerson() {
		return new PersonBuilder();
	}
}
