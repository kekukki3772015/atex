package person;

public class PersonBuilder {

	private String name = "Tiit";
	private Address address = new Address("Tallinn", "Raekojaplats 1");
	private String gender = Person.GENDER_MALE;
	private int age = 17;

	public PersonBuilder withAge(int age) {
		this.age = age;
		return this;
	}

	public PersonBuilder withGender(String gender) {
		this.gender = gender;
		return this;
	}

	public PersonBuilder withTown(String town) {
		address.setTown(town);
		return this;
	}

	public Person build() {
		Person p = new Person(name, age, gender, address);
		p.setName(name);
		p.setAge(age);
		p.setGender(gender);
		p.setAddress(address);
		return p;
	}
}
